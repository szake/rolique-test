$(document).ready(function() {

  showSiteOverlayElems();
  initPagePiling();

});
$(window).load(function() {

  // Added timeout 3 seconds
  setTimeout( hideSiteOverlay, 3000);

});

/* Functions */

/* Sections scrolling */
var sections = $('.site-section');

function initPagePiling() {

  $('.site-content').pagepiling({
    menu: '.site-navigation',
    verticalCentered: true,
    anchors: ['introduction', 'our-work', 'who-we-are', 'contact-us'],
    scrollingSpeed: 700,
    easing: 'swing',
    navigation: false,
    normalScrollElements: null,
    sectionSelector: '.site-section',

    //events
    onLeave: function(index, nextIndex, direction){
      console.log(direction);
    },
    afterLoad: function(anchorLink, index){
      console.log(anchorLink);
      console.log(index);
    },
    afterRender: function(){}
  });
}

/* Overlay */
function showSiteOverlayElems() {
  $('.site-overlay-icon').fadeIn(500, function(){
    $('.site-overlay-circle').fadeIn(200);
  });
}
function hideSiteOverlay() {
  // When page is loaded

  $('.site-overlay-circle').fadeOut(200, function(){
    $('.site-overlay-icon').fadeOut(500, function(){
      $('.site-overlay').fadeOut(1000);
    });
  });
}